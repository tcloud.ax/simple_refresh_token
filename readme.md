# Simple Refresh Token

Symfony bundle для работы быстрой и простой генерации Refresh токена.

![version](https://img.shields.io/badge/version-v1.0.0-blue) ![docs](https://img.shields.io/badge/docs-yes-blue)  ![license](https://img.shields.io/badge/license-MIT-brightgreen) ![useful](https://img.shields.io/badge/Maintained%3F-yes-brightgreen)


## Установка

```
composer require tcloud.ax/simple-refresh-token
```

## Использование

[Пример генерации refresh токена](https://gitlab.com/tcloud.ax/simple_refresh_token/-/blob/master/docs/refresh_token_generation_example.md)

[Пример получения refresh токена через экстракторы](https://gitlab.com/tcloud.ax/simple_refresh_token/-/blob/master/docs/refresh_token_extraction_example.md)

[Конфигуцрация менеджера и доступные опции](https://gitlab.com/tcloud.ax/simple_refresh_token/-/blob/master/docs/refresh_token_configuration.md)

[Пользовательский генератор refresh токена](https://gitlab.com/tcloud.ax/simple_refresh_token/-/blob/master/docs/custom_refresh_token_generator.md)

[Пользовательские экстракторы refresh токена](https://gitlab.com/tcloud.ax/simple_refresh_token/-/blob/master/docs/custom_refresh_token_extractor.md)