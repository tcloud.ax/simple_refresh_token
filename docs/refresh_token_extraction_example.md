# Пример получения refresh токена через экстракторы

По умолчанию в менеджере инициализируется ChainExtractor, который в свою очередь создаст несколько стандартный экстракторов (Request/Header/Query/Cookie-Extractor). Запустит метод `extract(Request $request)` и в цикле будет пробовать получить токен каждый имеющимся экстрактором. Первый же не пустой результат будет отдан.

Пример:
```php
<?php

namespace App\Controller;

use SimpleRefreshToken\RefreshTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/", name="")
     */
    public function index(Request $request, RefreshTokenManagerInterface $refreshTokenManager): Response
    {
        $refreshToken = $refreshTokenManager->extract($request);
        if (null !== $refreshToken) {
            return $this->json([
                'status' => 'success'
            ]);
        }

        return $this->json([
            'status' => 'forbidden'
        ]);
    }
}
```