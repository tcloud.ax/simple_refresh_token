# Конфигуцрация менеджера и доступные опции

Доступные опции

```yaml
# config/simple_refresh_token.yaml
simple_refresh_token:
  ttl: 0
  parameter_name: refresh_token
  header_name: Authorization
  refresh_token_class: SimpleRefreshToken\RefreshToken
  generator_class: SimpleRefreshToken\Generator\RefreshTokenGenerator
  extractors:
    - SimpleRefreshToken\Extractor\ChainExtractor
    - App\Extractor\MyCustomExtractorClass
```

### Параметр ttl
Время жизни токена, если указать меньше 1, то токен будет жить вечно.

### Параметр parameter_name
Название параметра в теле запроса, в параметрах запроса и в куках.

### Параметр header_name
Название заголовка, откуда будет взят токен при использовании HeaderExtractor.

### Параметр refresh_token_class
Ваш кастомный класс имплементирующий RefreshTokenInterface.

### Параметр generator_class
Класс генератора токена, если нужно изменить алгоритм генерации токена.

### Параметр extractors
Массив неймспейсов ваших экстракторов.