# Пользовательские экстракторы refresh токена

Для использования дополнительной логики при получении рефреш токена из объекта запроса вы можете создать собственные екстракторы.

```yaml
simple_refresh_token:
  extractors:
    - App\Extractor\BearerTokenExtractor
```

```php
<?php

declare(strict_types=1);

namespace App\Extractor;

use SimpleRefreshToken\Extractor\AbstractExtractor;
use Symfony\Component\HttpFoundation\Request;

class BearerTokenExtractor extends AbstractExtractor
{
    public const PREFIX = 'Bearer ';

    public function extract(Request $request): ?string
    {
        return substr(
            $request->headers->get($this->config->getHeaderNameOption()->getValue()),
            strlen(self::PREFIX)
        );
    }
}
```