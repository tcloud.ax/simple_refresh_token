# Пример генерации refresh токена

Генерация токена происходит через `\SimpleRefreshToken\RefreshTokenManagerInterface` метод create. Возвращает `\SimpleRefreshToken\RefreshTokenInterface`.

Пример с контроллером и сервис менеджером симфони:

```php
<?php

namespace App\Controller;

use SimpleRefreshToken\RefreshTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/", name="")
     */
    public function index(RefreshTokenManagerInterface $refreshTokenManager): Response
    {
        // output: {"refresh_token": "<any generated string>"}
        return $this->json([
            'refresh_token' => $refreshTokenManager->create(),
        ]);
    }
}
```