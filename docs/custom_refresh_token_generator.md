# Пользовательский генератор refresh токена

В тех случаях, когда вам нужно использовать собственный алгоритм генерации рефреш токена вы можете создать собственный генератор токена и указать его неймспейс в конфиг файле. Для того чтобы использовать остальные опции конфигурации можно прокинуть `RefreshTokenManagerConfig` в конструктор вашего генератора.

```yaml
// config/packages/simple_refresh_token.yaml
simple_refresh_token:
  generator_class: App\Generator\RefreshTokenGenerator
```

```php
<?php

declare(strict_types=1);

namespace App\Generator;

use SimpleRefreshToken\Configuration\RefreshTokenManagerConfig;
use SimpleRefreshToken\Generator\RefreshTokenGeneratorInterface;
use SimpleRefreshToken\RefreshToken;
use SimpleRefreshToken\RefreshTokenInterface;

class RefreshTokenGenerator implements RefreshTokenGeneratorInterface
{
    private RefreshTokenManagerConfig $config;

    /**
     * @param Config $config
     */
    public function __construct(RefreshTokenManagerConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @return RefreshTokenInterface
     */
    public function create(): RefreshTokenInterface
    {
        return new RefreshToken($this->generateToken(), $this->config->getRefreshTTLOption()->getValue());
    }

    /**
     * @return string
     */
    public function generateToken(): string
    {
        return hash('sha256', random_bytes(256));
    }
}
```