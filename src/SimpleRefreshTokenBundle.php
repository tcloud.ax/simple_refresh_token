<?php

declare(strict_types=1);

namespace SimpleRefreshToken;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use SimpleRefreshToken\DependencyInjection\SimpleRefreshTokenExtension;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
class SimpleRefreshTokenBundle extends Bundle
{
    /**
     * @return SimpleRefreshTokenExtension
     */
    public function getContainerExtension()
    {
        return new SimpleRefreshTokenExtension();
    }
}
