<?php

declare(strict_types=1);

namespace SimpleRefreshToken;

use Symfony\Component\HttpFoundation\Request;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
interface RefreshTokenManagerInterface
{
    /**
     * @return RefreshTokenInterface
     */
    public function create(): RefreshTokenInterface;

    /**
     * @return string|null
     */
    public function extract(Request $request): ?string;
}