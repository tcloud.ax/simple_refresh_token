<?php

declare(strict_types=1);

namespace SimpleRefreshToken\Exception;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
class SimpleRefreshTokenException extends \Exception
{
    // ...
}