<?php

declare(strict_types=1);

namespace SimpleRefreshToken;

use DateTime;
use DateTimeInterface;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
class RefreshToken implements RefreshTokenInterface
{
    protected string $token;
    protected ?DateTimeInterface $expires;
    protected int $ttl;
    protected bool $isInfinite;

    /**
     * @param string  $token
     * @param integer $ttl
     */
    public function __construct(string $token, int $ttl)
    {
        $this->token      = $token;
        $this->ttl        = $ttl;
        $this->expires    = null;
        $this->isInfinite = false;
        if ($ttl > 0) {
            $this->expires = new DateTime();
            $this->expires->modify(sprintf('+%s seconds', $ttl));
        } else {
            $this->isInfinite = true;
        }
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }
    
    /**
     * @return DateTimeInterface|null
     */
    public function getExpires(): ?DateTimeInterface
    {
        return $this->expires;
    }
    
    /**
     * @return integer
     */
    public function getTokenTTL(): int
    {
        return $this->ttl;
    }
    
    /**
     * @return boolean
     */
    public function isExpired(): bool
    {
        $now = new DateTime();

        return $now->getTimestamp() > $this->expires->getTimestamp();
    }

    /**
     * @return boolean
     */
    public function isInfinite(): bool
    {
        return $this->isInfinite;
    }
}