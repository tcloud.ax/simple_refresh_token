<?php

declare(strict_types=1);

namespace SimpleRefreshToken\Generator;

use SimpleRefreshToken\Configuration\Config;
use SimpleRefreshToken\Configuration\RefreshTokenManagerConfig;
use SimpleRefreshToken\RefreshToken;
use SimpleRefreshToken\RefreshTokenInterface;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
class RefreshTokenGenerator implements RefreshTokenGeneratorInterface
{
    private RefreshTokenManagerConfig $config;

    /**
     * @param Config $config
     */
    public function __construct(RefreshTokenManagerConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @return RefreshTokenInterface
     */
    public function create(): RefreshTokenInterface
    {
        $token = $this->createRandomString();
        $ttl   = $this->config->getRefreshTTLOption()->getValue();

        return new RefreshToken($token, $ttl);
    }

    /**
     * @return string
     */
    protected function createRandomString(): string
    {
        return bin2hex(random_bytes(64));
    }
}