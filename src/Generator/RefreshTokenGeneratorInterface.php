<?php

declare(strict_types=1);

namespace SimpleRefreshToken\Generator;

use SimpleRefreshToken\RefreshTokenInterface;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
interface RefreshTokenGeneratorInterface
{
    /**
     * @return RefreshTokenInterface
     */
    public function create(): RefreshTokenInterface;
}