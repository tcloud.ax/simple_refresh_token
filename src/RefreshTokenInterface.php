<?php
declare(strict_types=1);

namespace SimpleRefreshToken;

use DateTimeInterface;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
interface RefreshTokenInterface
{
    /**
     * @return string
     */
    public function getToken(): string;
    
    /**
     * @return DateTimeInterface|null
     */
    public function getExpires(): ?DateTimeInterface;
    
    /**
     * @return integer
     */
    public function getTokenTTL(): int;
    
    /**
     * @return boolean
     */
    public function isExpired(): bool;
}