<?php

declare(strict_types=1);

namespace SimpleRefreshToken\Factory;

use InvalidArgumentException;
use SimpleRefreshToken\Configuration\RefreshTokenManagerConfig;
use SimpleRefreshToken\Extractor\ChainExtractor;
use SimpleRefreshToken\Extractor\CookieExtractor;
use SimpleRefreshToken\Extractor\RefreshTokenExtractorInterface;
use SimpleRefreshToken\Extractor\HeaderExtractor;
use SimpleRefreshToken\Extractor\QueryExtractor;
use SimpleRefreshToken\Extractor\RequestExtractor;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
class ExtractorsFactory
{
    /**
     * @param RefreshTokenManagerConfig $config
     * 
     * @return RequestExtractor
     */
    public function createRequestExtractor(RefreshTokenManagerConfig $config): RequestExtractor
    {
        return new RequestExtractor($config);
    }

    /**
     * @param RefreshTokenManagerConfig $config
     * 
     * @return QueryExtractor
     */
    public function createQueryExtractor(RefreshTokenManagerConfig $config): QueryExtractor
    {
        return new QueryExtractor($config);
    }

    /**
     * @param RefreshTokenManagerConfig $config
     * 
     * @return CookieExtractor
     */
    public function createCookieExtractor(RefreshTokenManagerConfig $config): CookieExtractor
    {
        return new CookieExtractor($config);
    }

    /**
     * @param RefreshTokenManagerConfig $config
     * 
     * @return HeaderExtractor
     */
    public function createHeaderExtractor(RefreshTokenManagerConfig $config): HeaderExtractor
    {
        return new HeaderExtractor($config);
    }

    /**
     * @param RefreshTokenManagerConfig $config
     * @param ExtractorsFactory         $factory
     * 
     * @return ChainExtractor
     */
    public function  createChainExtractor(RefreshTokenManagerConfig $config, ExtractorsFactory $factory): ChainExtractor
    {
        return new ChainExtractor($config, $factory);
    }

    /**
     * @param string                    $class
     * @param RefreshTokenManagerConfig $config
     * 
     * @return RefreshTokenExtractorInterface
     * 
     * @throws InvalidArgumentException
     */
    public function create(string $class, RefreshTokenManagerConfig $config): RefreshTokenExtractorInterface
    {
        switch ($class) {
            case RequestExtractor::class:
                return $this->createRequestExtractor($config);
            case QueryExtractor::class:
                return $this->createQueryExtractor($config);
            case CookieExtractor::class:
                return $this->createCookieExtractor($config);
            case HeaderExtractor::class:
                return $this->createHeaderExtractor($config);
            default:
                throw new InvalidArgumentException(
                    sprintf('Extractor class %s not found.', $class)
                );
                break;
        }
    }
}