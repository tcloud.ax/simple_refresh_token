<?php

declare(strict_types=1);

namespace SimpleRefreshToken\Configuration;

use SimpleRefreshToken\Configuration\Options\ExtractorsOption;
use SimpleRefreshToken\Configuration\Options\GeneratorClassOption;
use SimpleRefreshToken\Configuration\Options\HeaderNameOption;
use SimpleRefreshToken\Configuration\Options\ParameterNameOption;
use SimpleRefreshToken\Configuration\Options\RefreshTTLOption;
use SimpleRefreshToken\Configuration\Options\TokenClassOption;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder(RefreshTokenManagerConfig::CONFIG_NAME);
        $treeBuilder->getRootNode()
            ->children()
                ->integerNode(RefreshTTLOption::OPTION_NAME)->end()
                ->scalarNode(ParameterNameOption::OPTION_NAME)->end()
                ->scalarNode(HeaderNameOption::OPTION_NAME)->end()
                ->scalarNode(TokenClassOption::OPTION_NAME)->end()
                ->scalarNode(GeneratorClassOption::OPTION_NAME)->end()
                ->arrayNode(ExtractorsOption::OPTION_NAME)
                    ->scalarPrototype()->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}