<?php

declare(strict_types=1);

namespace SimpleRefreshToken\Configuration\Options;

use SimpleRefreshToken\RefreshToken;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
class TokenClassOption
{
    public const OPTION_NAME = 'token_class';
    public const DEFAULT_VALUE = RefreshToken::class;

    private string $value;

    /**
     * @param string|null $value
     */
    public function __construct(?string $value = self::DEFAULT_VALUE)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}