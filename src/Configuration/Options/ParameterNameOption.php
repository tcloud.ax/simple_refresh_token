<?php

declare(strict_types=1);

namespace SimpleRefreshToken\Configuration\Options;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
class ParameterNameOption
{
    public const OPTION_NAME = 'parameter_name';
    public const DEFAULT_VALUE = 'refresh_token';

    private string $value;

    /**
     * @param string|null $value
     */
    public function __construct(?string $value = self::DEFAULT_VALUE)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}