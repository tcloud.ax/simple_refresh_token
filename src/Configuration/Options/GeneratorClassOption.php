<?php

declare(strict_types=1);

namespace SimpleRefreshToken\Configuration\Options;

use SimpleRefreshToken\Generator\RefreshTokenGenerator;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
class GeneratorClassOption
{
    public const OPTION_NAME = 'generator_class';
    public const DEFAULT_VALUE = RefreshTokenGenerator::class;

    private string $value;

    /**
     * @param string|null $value
     */
    public function __construct(?string $value = self::DEFAULT_VALUE)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}