<?php

declare(strict_types=1);

namespace SimpleRefreshToken\Configuration\Options;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
final class RefreshTTLOption
{
    public const OPTION_NAME = 'ttl';
    public const DEFAULT_VALUE = -1;

    private int $value;

    /**
     * @param integer|null $value = self::DEFAULT_VALUE
     */
    public function __construct(?int $value = self::DEFAULT_VALUE)
    {
        $this->value = $value;
    }

    /**
     * @return integer
     */
    public function getValue(): int
    {
        return $this->value;
    }
}