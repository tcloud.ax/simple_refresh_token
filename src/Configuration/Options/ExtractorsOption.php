<?php

declare(strict_types=1);

namespace SimpleRefreshToken\Configuration\Options;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
class ExtractorsOption
{
    public const OPTION_NAME = 'extractors';
    public const DEFAULT_VALUE = [];

    private array $value;

    /**
     * @param string[] $extractors
     */
    public function __construct(array $value = self::DEFAULT_VALUE)
    {
        $this->value = $value;
    }

    /**
     * @return string[]
     */
    public function getValue(): array
    {
        return $this->value;
    }
}