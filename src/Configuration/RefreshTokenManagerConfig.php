<?php

declare(strict_types=1);

namespace SimpleRefreshToken\Configuration;

use SimpleRefreshToken\Configuration\Options\ExtractorsOption;
use SimpleRefreshToken\Configuration\Options\GeneratorClassOption;
use SimpleRefreshToken\Configuration\Options\HeaderNameOption;
use SimpleRefreshToken\Configuration\Options\ParameterNameOption;
use SimpleRefreshToken\Configuration\Options\RefreshTTLOption;
use SimpleRefreshToken\Configuration\Options\TokenClassOption;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
/**
 * @author tcloud <tcloud.ax@gmail.com>
 */
class RefreshTokenManagerConfig
{
    public const CONFIG_NAME = 'simple_refresh_token';

    private RefreshTTLOption $refreshTTLOption;
    private ParameterNameOption $parameterNameOption;
    private HeaderNameOption $headerNameOption;
    private TokenClassOption $tokenClassOption;
    private GeneratorClassOption $generatorClassOption;
    private ExtractorsOption $extractorsOption;

    /**
     * @param RefreshTTLOption     $refreshTTLOption
     * @param ParameterNameOption  $parameterNameOption
     * @param HeaderNameOption     $headerNameOption
     * @param TokenClassOption     $tokenClassOption
     * @param GeneratorClassOption $generatorClassOption
     * @param ExtractorsOption     $extractorsOption
     */
    public function __construct(
        RefreshTTLOption $refreshTTLOption,
        ParameterNameOption $parameterNameOption,
        HeaderNameOption $headerNameOption,
        TokenClassOption $tokenClassOption,
        GeneratorClassOption $generatorClassOption,
        ExtractorsOption $extractorsOption
    ) {
        $this->refreshTTLOption     = $refreshTTLOption;
        $this->parameterNameOption  = $parameterNameOption;
        $this->headerNameOption     = $headerNameOption;
        $this->tokenClassOption     = $tokenClassOption;
        $this->generatorClassOption = $generatorClassOption;
        $this->extractorsOption     = $extractorsOption;
    }

    /**
     * @return RefreshTTLOption
     */
    public function getRefreshTTLOption(): RefreshTTLOption
    {
        return $this->refreshTTLOption;
    }

    /**
     * @return ParameterNameOption
     */
    public function getParameterNameOption(): ParameterNameOption
    {
        return $this->parameterNameOption;
    }

    /**
     * @return HeaderNameOption
     */
    public function getHeaderNameOption(): HeaderNameOption
    {
        return $this->headerNameOption;
    }

    /**
     * @return TokenClassOption
     */
    public function getTokenClassOption(): TokenClassOption
    {
        return $this->tokenClassOption;
    }

    /**
     * @return GeneratorClassOption
     */
    public function getGeneratorClassOption(): GeneratorClassOption
    {
        return $this->generatorClassOption;
    }

    /**
     * @return ExtractorsOption
     */
    public function getExtractorsOption(): ExtractorsOption
    {
        return $this->extractorsOption;
    }
}