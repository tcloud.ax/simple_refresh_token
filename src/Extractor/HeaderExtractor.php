<?php

declare(strict_types=1);

namespace SimpleRefreshToken\Extractor;

use Symfony\Component\HttpFoundation\Request;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
class HeaderExtractor extends AbstractExtractor
{
    /**
     * @param Request $request
     * 
     * @return string|null
     */
    public function extract(Request $request): ?string
    {
        return $request->headers->get($this->config->getHeaderNameOption()->getValue());
    }
}