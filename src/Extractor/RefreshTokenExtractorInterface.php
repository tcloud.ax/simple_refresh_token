<?php

declare(strict_types=1);

namespace SimpleRefreshToken\Extractor;

use Symfony\Component\HttpFoundation\Request;

/**
 * Экстракторы не должны сохранять какое то состояние или использовать какие либо зависимости.
 * Они должны только имлементировать extract(Symfony\Component\HttpFoundation\Request $request) метод.
 * 
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 * @see    \Symfony\Component\HttpFoundation\Request
 */
interface RefreshTokenExtractorInterface
{
    /**
     * @param Request $request
     * 
     * @return string|null
     */
    public function extract(Request $request): ?string;
}