<?php

declare(strict_types=1);

namespace SimpleRefreshToken\Extractor;

use SimpleRefreshToken\Configuration\RefreshTokenManagerConfig;
use SimpleRefreshToken\Factory\ExtractorsFactory;
use Symfony\Component\HttpFoundation\Request;

/**
 * Агрегатор экстракторов, запускает экстракторы по установленому по умолачнию порядку.
 * 
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
class ChainExtractor extends AbstractExtractor
{
    protected array $extractors;
    protected ExtractorsFactory $extractorsFactory;

    /**
     * @param RefreshTokenManagerConfig $config
     * @param ExtractorsFactory         $extractorsFactory
     */
    public function __construct(RefreshTokenManagerConfig $config, ExtractorsFactory $extractorsFactory)
    {
        parent::__construct($config);
        $this->extractorsFactory = $extractorsFactory;
    }

    /**
     * @param Request $request
     * 
     * @return string|null
     */
    public function extract(Request $request): ?string
    {
        $extractors = $this->getDefaultExtractors();
        $result     = null;
        foreach ($extractors as $class) {
            $extractor = $this->getExtractor($class);
            $result    = $extractor->extract($request);
            if (null !== $result) {
                break;
            }
        }

        return $result;
    }

    /**
     * @return string[]
     */
    protected function getDefaultExtractors(): array
    {
        return [
            HeaderExtractor::class,
            RequestExtractor::class,
            CookieExtractor::class,
            QueryExtractor::class,
        ];
    }

    /**
     * @param string $class
     * 
     * @return RefreshTokenExtractorInterface
     */
    protected function getExtractor(string $class): RefreshTokenExtractorInterface
    {
        return $this->extractorsFactory->create($class, $this->config);
    }
}