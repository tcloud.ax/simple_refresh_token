<?php

declare(strict_types=1);

namespace SimpleRefreshToken\Extractor;

use SimpleRefreshToken\Configuration\RefreshTokenManagerConfig;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
abstract class AbstractExtractor implements RefreshTokenExtractorInterface
{
    protected RefreshTokenManagerConfig $config;

    /**
     * @param RefreshTokenManagerConfig $config
     */
    public function __construct(RefreshTokenManagerConfig $config)
    {
        $this->config  = $config;
    }

    /**
     * @param Request $request
     * 
     * @return string|null
     */
    abstract public function extract(Request $request): ?string;
}