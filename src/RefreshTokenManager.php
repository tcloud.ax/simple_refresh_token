<?php

declare(strict_types=1);

namespace SimpleRefreshToken;

use SimpleRefreshToken\Configuration\RefreshTokenManagerConfig;
use SimpleRefreshToken\Exception\SimpleRefreshTokenException;
use SimpleRefreshToken\Extractor\RefreshTokenExtractorInterface;
use SimpleRefreshToken\Factory\ExtractorsFactory;
use SimpleRefreshToken\Generator\RefreshTokenGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
class RefreshTokenManager implements RefreshTokenManagerInterface
{
    protected RefreshTokenManagerConfig $config;
    protected ExtractorsFactory $extractorsFactory;
    protected RefreshTokenGeneratorInterface $generator;
    /** @var RefreshTokenExtractorInterface[] */
    protected array $extractors;

    /**
     * @param RefreshTokenManagerConfig $config
     */
    public function __construct(RefreshTokenManagerConfig $config)
    {
        $this->config            = $config;
        $this->extractorsFactory = new ExtractorsFactory();
        
        $this->initializeGenerator();
        $this->initializeExtractors();
    }
    
    /**
     * @return void
     * 
     * @throws SimpleRefreshTokenException
     */
    protected function initializeGenerator(): void
    {
        $generatorClass = $this
            ->config
            ->getGeneratorClassOption()
            ->getValue()
        ;
        $generator      = new $generatorClass($this->config);
        if ($generator instanceof RefreshTokenGeneratorInterface) {
            $this->generator = $generator;
            return;
        }
        throw new SimpleRefreshTokenException();
    }

    /**
     * @return void
     * 
     * @throws SimpleRefreshTokenException
     */
    protected function initializeExtractors(): void
    {
        $customExtractors = $this->config->getExtractorsOption()->getValue();
        $extractors = [];
        if (empty($customExtractors)) {
            $extractors[] = $this->extractorsFactory->createChainExtractor(
                $this->config,
                $this->extractorsFactory
            );
        } else {
            foreach ($customExtractors as $class) {
                $extractor = new $class($this->config);
                if (!$extractor instanceof RefreshTokenExtractorInterface) {
                    throw new SimpleRefreshTokenException(
                        sprintf('Extractor %s is not instance of RefreshTokenExtractorInterface', $class)
                    );
                }
                $extractors[] = $extractor;
            }
        }
        $this->extractors = $extractors;
    }

    /**
     * @return RefreshTokenInterface
     */
    public function create(): RefreshTokenInterface
    {
        return $this->generator->create();
    }

    /**
     * @param Request $request
     * 
     * @return string|null
     */
    public function extract(Request $request): ?string
    {
        $result = null;
        foreach ($this->extractors as $extractor) {
            $result = $extractor->extract($request);
            if (null !== $result) {
                break;
            }
        }

        return $result;
    }

    /**
     * @return RefreshTokenManagerConfig
     */
    public function getConfig(): RefreshTokenManagerConfig
    {
        return $this->config;
    }
}